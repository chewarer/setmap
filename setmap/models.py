from __future__ import unicode_literals
from django.db import models


class MapData(models.Model):
	# id = models.IntegerField(primary_key=True, unique=True,
	#                          db_index=True, auto_created=True)
	name = models.CharField(max_length=50)
	data_yandex = models.CharField(max_length=50, blank=True)

	def __str__(self):
		return str(self.name)
