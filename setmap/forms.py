from django import forms
from .models import MapData


class MapForm(forms.ModelForm):

	class Meta:
		model = MapData
		fields = ('data_yandex',)
