from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.address, name='address'),
	# url(r'^map', views.yamap, name='yamap_edit'),
	url(r'^map/(?P<pk>[0-9]+)/edit/$', views.yamap_edit, name='yamap_edit'),
]
