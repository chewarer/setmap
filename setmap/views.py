from django.shortcuts import render, redirect, get_object_or_404
from .models import MapData
from .forms import MapForm


def address(request):
	coord = MapData.objects.all()
	return render(request,
	              'setmap/address.html',
	              {'coord': coord})


# def yamap(request):
# 	return render(request,
# 	              'setmap/map.html')


def yamap_edit(request, pk):
	coord = get_object_or_404(MapData, pk=pk)
	if request.method == 'POST':
		form = MapForm(request.POST, instance=coord)
		if form.is_valid():
			coord.save()
			return redirect('setmap.views.yamap_edit', pk=coord.pk)
	else:
		form = MapForm(instance=coord)
	return render(request, 'setmap/map.html', {'form': form})
