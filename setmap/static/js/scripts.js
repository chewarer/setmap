	$(document).ready(function(){

        //Открытие нового окна
        $(".coordinates").click(function(e) {
            e.preventDefault();
            var field_id = this.id,
                height = screen.availHeight*0.8,
                width = screen.availWidth*0.8,
                w = window.open('', '',
                                'scrollbars=1,' +
                                'height='+Math.min(height, screen.availHeight)+',' +
                                'width='+Math.min(width, screen.availWidth)+',' +
                                'left='+Math.max(0, (screen.availWidth - width)/2)+',' +
                                'top='+Math.max(0, (screen.availHeight - height)/2));
            //alert(field_id);
            $.ajax({
                url: "/map/"+field_id+"/edit",
                async: false,
                success: function() {
                    w.location.href="/map/"+field_id+"/edit?"+field_id;
                }
            });
        });

        //Устанавливаем координаты в родительском окне в нужный input
        function setValue(coords, address) {
            $('.map-page .message').slideDown('slow');
            var field_id= window.location.search.replace( '?', '');
            $('.map-page .address').html(address);
            $('#id_data_yandex').val(coords);
            $('.map-page .confirm').click(function() {
                //Записываем координаты в вызвавшее поле в родительском окне
                //window.opener.document.getElementById(field_id).value = coords; window.close();
                $(window.opener.document).find('#'+field_id).parent().addClass('isset'); window.close();
            });
        }



        //Карта
        ymaps.ready(init);

        function init() {
            var myPlacemark,
                myMap = new ymaps.Map('map', {
                    center: [56.010563284002934,92.85342387448364],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                });

            // Слушаем клик на карте.
            myMap.events.add('click', function (e) {
                var coords = e.get('coords');

                // Если метка уже создана – просто передвигаем ее.
                if (myPlacemark) {
                    myPlacemark.geometry.setCoordinates(coords);
                }
                // Если нет – создаем.
                else {
                    myPlacemark = createPlacemark(coords);
                    myMap.geoObjects.add(myPlacemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    myPlacemark.events.add('dragend', function () {
                        getAddress(myPlacemark.geometry.getCoordinates());
                    });
                }
                getAddress(coords);

            });

            // Создание метки.
            function createPlacemark(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: 'поиск...'
                }, {
                    preset: 'islands#violetDotIconWithCaption',
                    draggable: true
                });
            }

            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                myPlacemark.properties.set('iconCaption', 'поиск...');
                ymaps.geocode(coords).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);

                    myPlacemark.properties
                        .set({
                            iconCaption: firstGeoObject.properties.get('name'),
                            balloonContent: firstGeoObject.properties.get('text')
                        });

                    var address = firstGeoObject.properties.get('text');

                    //Передаем указанные координаты дальше
                    setValue(coords, address);
                });
            }
        }


	});
